package com.tgs.warehouse.controller;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.tgs.warehouse.Application;
import com.tgs.warehouse.model.ProductPallet;
import com.tgs.warehouse.model.dto.AbstractCreationDTO;
import com.tgs.warehouse.model.dto.AbstractUpdateDTO;
import com.tgs.warehouse.model.dto.ProductPalletCreationDTO;
import com.tgs.warehouse.model.dto.ProductPalletUpdateDTO;
import com.tgs.warehouse.security.jwt.authentication.controller.AuthRestAPIs;
import com.tgs.warehouse.security.model.Role;
import com.tgs.warehouse.security.repository.RoleRepository;
import com.tgs.warehouse.service.IService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

import java.util.LinkedList;
import java.util.List;

import static com.tgs.warehouse.security.model.RoleName.ROLE_ADMIN;
import static com.tgs.warehouse.security.model.RoleName.ROLE_USER;
import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.http.MediaType.APPLICATION_JSON_UTF8;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = Application.class)
@TestPropertySource(
        locations = "classpath:application-test.properties")
@AutoConfigureMockMvc
@Transactional
public class PalletControllerTest {

    private String token;

    @Autowired
    AuthRestAPIs authRestAPIs;

    @Autowired
    RoleRepository roleRepository;



    @Autowired
    PalletController palletController;

    @Autowired
    MockMvc mvc;

    @Autowired
    ModelMapper mapper;

    @Autowired
    IService<ProductPallet> productPalletService;

    @Before
    public void beforeTest() throws Exception{
        Role admin=new Role();
        admin.setName(ROLE_ADMIN);
        roleRepository.save(admin);

        Role user=new Role();
        user.setName(ROLE_USER);
        roleRepository.save(user);

        String jsonUserSignUp="{\n" +
                "\t\"name\":\"Test User\",\n" +
                "\t\"username\":\"test\",\n" +
                "\t\"email\":\"test@yahoo.com\",\n" +
                "\t\"role\":[\"admin\"],\n" +
                "\t\"password\":\"12345678\"\n" +
                "}";

        mvc.perform(post("/api/auth/signup")
                .contentType(APPLICATION_JSON).content(jsonUserSignUp))
                .andReturn().getResponse().getContentAsString();

        String jsonUserSignIn="{\n" +
                "\"username\":\"test\",\n" +
                "\"password\":\"12345678\"\n" +
                "}";

        String response = mvc.perform(post("/api/auth/signin")
                .contentType(MediaType.APPLICATION_JSON_UTF8).content(jsonUserSignIn))
                .andExpect(status().isOk()).andReturn().getResponse().getContentAsString();

        ObjectMapper objectMapper=new ObjectMapper();
        JsonNode actualObj = objectMapper.readTree(response);

        token="Bearer "+actualObj.get("accessToken").asText();

    }

    @Test()
    public void searchPalletByDescriptionAndWithEmptyDescriptionTest() throws Exception {
        AbstractCreationDTO<ProductPallet> palletCreationDTO = createNewPalletWithDescription();

        List<AbstractUpdateDTO<ProductPallet>> productPalletDTOs = new LinkedList<>();
        AbstractUpdateDTO<ProductPallet> palletUpdateDTO = mapper.map(productPalletService.save(palletCreationDTO), ProductPalletUpdateDTO.class);
        productPalletDTOs.add(palletUpdateDTO);

        String response = mvc.perform(get("/api/pallet/?description={description}", palletUpdateDTO.getDescription()).header("Authorization",token))
                .andExpect(status().isOk()).andReturn().getResponse().getContentAsString();
        String jsonObject = new ObjectMapper().writeValueAsString(productPalletDTOs);
        assertThat(response).isEqualTo(jsonObject);

        AbstractUpdateDTO<ProductPallet> auxPalletUpdateDTO = mapper.map(productPalletService.save(palletCreationDTO), ProductPalletUpdateDTO.class);
        productPalletDTOs.add(auxPalletUpdateDTO);
        response = mvc.perform(get("/api/pallet/?description=").header("Authorization",token))
                .andExpect(status().isOk()).andReturn().getResponse().getContentAsString();

        jsonObject = new ObjectMapper().writeValueAsString(productPalletDTOs);
        assertThat(response).isEqualTo(jsonObject);
    }

    @Test
    public void findOneProductPalletById() throws Exception {

        ProductPallet productPallet=mapper.map(productPalletService.save(createNewPalletWithDescription()),ProductPallet.class);

        String response = mvc.perform(get("/api/pallet/{id}", productPallet.getId()).header("Authorization",token))
                .andExpect(status().isOk()).andReturn().getResponse().getContentAsString();
        String jsonObject = new ObjectMapper().writeValueAsString(productPallet);
        assertThat(response).isEqualTo(jsonObject);
    }

    @Test
    public void deletePalletById() throws Exception {
        AbstractCreationDTO<ProductPallet> palletCreationDTO = createNewPalletWithDescription();

        palletCreationDTO = productPalletService.save(palletCreationDTO);

        String response = mvc.perform(delete("/api/pallet/{id}", palletCreationDTO.getId()).header("Authorization",token))
                .andExpect(status().isOk()).andReturn().getResponse().getContentAsString();

        assertThat(response).isEqualTo("Pallet with id:" + palletCreationDTO.getId() + " has been deleted");
    }

    @Test
    public void insertPalletTest() throws Exception {
        ObjectMapper objectMapper = new ObjectMapper();

        ProductPalletCreationDTO palletCreationDTO = createNewPalletWithDescription();

        String jsonObject = objectMapper.writeValueAsString(palletCreationDTO);

        String response = mvc.perform(post("/api/pallet").header("Authorization",token).contentType(APPLICATION_JSON_UTF8).content(jsonObject))
                .andExpect(status().isOk()).andReturn().getResponse().getContentAsString();

        palletCreationDTO.setId(objectMapper.readTree(response).get("id").asLong());

        assertThat(response).isEqualTo(objectMapper.writeValueAsString(palletCreationDTO));
    }

    @Test
    public void updatePalletTest() throws Exception {
        String before = "testbeforeupdate";
        String after = "testafterupdate";
        ObjectMapper objectMapper = new ObjectMapper();
        ProductPalletCreationDTO palletCreationDTO = new ProductPalletCreationDTO();
        palletCreationDTO.setDescription(before);

        AbstractUpdateDTO<ProductPallet> palletUpdateDTO = mapper.map(productPalletService.save(palletCreationDTO), ProductPalletUpdateDTO.class);
        palletUpdateDTO.setDescription(after);
        String jsonObject = objectMapper.writeValueAsString(palletUpdateDTO);

        String response = mvc.perform(put("/api/pallet").header("Authorization",token).contentType(APPLICATION_JSON_UTF8).content(jsonObject))
                .andExpect(status().isOk()).andReturn().getResponse().getContentAsString();

        assertThat(response).isEqualTo(jsonObject);

        ProductPallet ppFromDb = productPalletService.findOne(palletUpdateDTO.getId());
        palletUpdateDTO = mapper.map(ppFromDb, ProductPalletUpdateDTO.class);
        String jsonPalletFromDb = objectMapper.writeValueAsString(palletUpdateDTO);

        assertThat(jsonPalletFromDb).isEqualTo(jsonObject);
    }

    private ProductPalletCreationDTO createNewPalletWithDescription() {
        ProductPalletCreationDTO palletCreationDTO = new ProductPalletCreationDTO();
        palletCreationDTO.setDescription("testdescription");
        return palletCreationDTO;
    }
}

