package com.tgs.warehouse.controller;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.tgs.warehouse.Application;
import com.tgs.warehouse.model.PlannedShipment;
import com.tgs.warehouse.model.dto.PlannedShipmentDTO;
import com.tgs.warehouse.security.jwt.authentication.controller.AuthRestAPIs;
import com.tgs.warehouse.security.model.Role;
import com.tgs.warehouse.security.repository.RoleRepository;
import com.tgs.warehouse.service.PlannedShipmentService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

import static com.tgs.warehouse.security.model.RoleName.ROLE_ADMIN;
import static com.tgs.warehouse.security.model.RoleName.ROLE_USER;
import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.http.MediaType.APPLICATION_JSON_UTF8;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = Application.class)
@TestPropertySource(
        locations = "classpath:application-test.properties")
@AutoConfigureMockMvc
@Transactional
public class PlannedShipmentControllerTest {
    private String token;

    @Autowired
    AuthRestAPIs authRestAPIs;

    @Autowired
    RoleRepository roleRepository;

    @Autowired
    MockMvc mvc;

    @Autowired
    ModelMapper mapper;

    @Autowired
    ObjectMapper objectMapper;

    @Autowired
    PlannedShipmentController plannedShipmentController;

    @Autowired
    PlannedShipmentService plannedShipmentService;

    @Before
    public void beforeTest() throws Exception {
        Role admin = new Role();
        admin.setName(ROLE_ADMIN);
        roleRepository.save(admin);

        Role user = new Role();
        user.setName(ROLE_USER);
        roleRepository.save(user);

        String jsonUserSignUp = "{\n" +
                "\t\"name\":\"Test User\",\n" +
                "\t\"username\":\"test\",\n" +
                "\t\"email\":\"test@yahoo.com\",\n" +
                "\t\"role\":[\"admin\"],\n" +
                "\t\"password\":\"12345678\"\n" +
                "}";

        mvc.perform(post("/api/auth/signup")
                .contentType(APPLICATION_JSON).content(jsonUserSignUp))
                .andReturn().getResponse().getContentAsString();

        String jsonUserSignIn = "{\n" +
                "\"username\":\"test\",\n" +
                "\"password\":\"12345678\"\n" +
                "}";

        String response = mvc.perform(post("/api/auth/signin")
                .contentType(MediaType.APPLICATION_JSON_UTF8).content(jsonUserSignIn))
                .andExpect(status().isOk()).andReturn().getResponse().getContentAsString();

        ObjectMapper objectMapper = new ObjectMapper();
        JsonNode actualObj = objectMapper.readTree(response);

        token = "Bearer " + actualObj.get("accessToken").asText();

    }

    @Test
    public void listAll() throws Exception {
        PlannedShipment plannedShipment = new PlannedShipment();
        plannedShipment.setCustomerName("test1");
        plannedShipment.setQuantity(10L);

        PlannedShipmentDTO firstDto = plannedShipmentService.save(mapper.map(plannedShipment, PlannedShipmentDTO.class));
        PlannedShipmentDTO secondDto = plannedShipmentService.save(mapper.map(plannedShipment, PlannedShipmentDTO.class));

        List<PlannedShipmentDTO> plannedShipmentDTOList = new ArrayList<>();
        plannedShipmentDTOList.add(firstDto);
        plannedShipmentDTOList.add(secondDto);

        String jsonObjects = new ObjectMapper().writeValueAsString(plannedShipmentDTOList);

        String response = mvc.perform(get("/api/shipmentplan/all").header("Authorization", token))
                .andExpect(status().isOk()).andReturn().getResponse().getContentAsString();

        assertThat(response).isEqualTo(jsonObjects);
    }

    @Test
    public void getById() throws Exception {
        PlannedShipment plannedShipment = new PlannedShipment();
        plannedShipment.setCustomerName("test1");
        plannedShipment.setQuantity(10L);

        PlannedShipmentDTO plannedShipmentDTO = plannedShipmentService.save(mapper.map(plannedShipment, PlannedShipmentDTO.class));
        String jsonObjects = new ObjectMapper().writeValueAsString(plannedShipmentDTO);

        String response = mvc.perform(get("/api/shipmentplan/{id}", plannedShipmentDTO.getId()).header("Authorization", token))
                .andExpect(status().isOk()).andReturn().getResponse().getContentAsString();

        assertThat(response).isEqualTo(jsonObjects);
    }

    @Test
    public void insert() throws Exception {
        PlannedShipment plannedShipment = new PlannedShipment();
        plannedShipment.setCustomerName("test1");
        plannedShipment.setQuantity(10L);

        PlannedShipmentDTO plannedShipmentDTO = mapper.map(plannedShipment, PlannedShipmentDTO.class);
        String jsonObject = new ObjectMapper().writeValueAsString(plannedShipmentDTO);

        String response = mvc.perform(post("/api/shipmentplan").contentType(MediaType.APPLICATION_JSON_UTF8).content(jsonObject).header("Authorization", token))
                .andExpect(status().isOk()).andReturn().getResponse().getContentAsString();

        plannedShipmentDTO.setId(objectMapper.readTree(response).get("id").asLong());
        String jsonObjectWithId = new ObjectMapper().writeValueAsString(plannedShipmentDTO);

        assertThat(response).isEqualTo(jsonObjectWithId);
    }

    @Test
    public void delete() throws Exception {
        PlannedShipment plannedShipment = new PlannedShipment();
        plannedShipment.setCustomerName("test1");
        plannedShipment.setQuantity(10L);

        PlannedShipmentDTO plannedShipmentDTO = plannedShipmentService.save(mapper.map(plannedShipment, PlannedShipmentDTO.class));

        String response = mvc.perform(MockMvcRequestBuilders.delete("/api/shipmentplan/{id}", plannedShipmentDTO.getId()).header("Authorization", token))
                .andExpect(status().isOk()).andReturn().getResponse().getContentAsString();

        assertThat(response).isEqualTo("Planned Shipment with id:" + plannedShipmentDTO.getId() + " has been deleted");
    }

    @Test
    public void update() throws Exception {
        PlannedShipment plannedShipment = new PlannedShipment();
        plannedShipment.setCustomerName("test1");
        plannedShipment.setQuantity(10L);

        PlannedShipmentDTO plannedShipmentDTO = plannedShipmentService.save(mapper.map(plannedShipment, PlannedShipmentDTO.class));
        plannedShipmentDTO.setCustomerName("AfterUpdate");
        plannedShipmentDTO.setQuantity(7L);
        String jsonObject = new ObjectMapper().writeValueAsString(plannedShipmentDTO);

        String response = mvc.perform(put("/api/shipmentplan").contentType(APPLICATION_JSON_UTF8).content(jsonObject).header("Authorization", token))
                .andExpect(status().isOk()).andReturn().getResponse().getContentAsString();

        assertThat(response).isEqualTo(jsonObject);
    }
}
