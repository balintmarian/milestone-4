package com.tgs.warehouse.controller;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.tgs.warehouse.Application;
import com.tgs.warehouse.model.Shipment;
import com.tgs.warehouse.model.dto.ShipmentDTO;
import com.tgs.warehouse.security.jwt.authentication.controller.AuthRestAPIs;
import com.tgs.warehouse.security.model.Role;
import com.tgs.warehouse.security.repository.RoleRepository;
import com.tgs.warehouse.service.ShipmentService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

import static com.tgs.warehouse.security.model.RoleName.ROLE_ADMIN;
import static com.tgs.warehouse.security.model.RoleName.ROLE_USER;
import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.http.MediaType.APPLICATION_JSON_UTF8;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = Application.class)
@TestPropertySource(
        locations = "classpath:application-test.properties")
@AutoConfigureMockMvc
@Transactional
public class ShipmentControllerTest {

    private String token;

    @Autowired
    AuthRestAPIs authRestAPIs;

    @Autowired
    RoleRepository roleRepository;

    @Autowired
    MockMvc mvc;

    @Autowired
    ModelMapper mapper;

    @Autowired
    ObjectMapper objectMapper;

    @Autowired
    ShipmentController shipmentController;

    @Autowired
    ShipmentService shipmentService;


    @Before
    public void beforeTest() throws Exception {
        Role admin = new Role();
        admin.setName(ROLE_ADMIN);
        roleRepository.save(admin);

        Role user = new Role();
        user.setName(ROLE_USER);
        roleRepository.save(user);

        String jsonUserSignUp = "{\n" +
                "\t\"name\":\"Test User\",\n" +
                "\t\"username\":\"test\",\n" +
                "\t\"email\":\"test@yahoo.com\",\n" +
                "\t\"role\":[\"admin\"],\n" +
                "\t\"password\":\"12345678\"\n" +
                "}";

        mvc.perform(post("/api/auth/signup")
                .contentType(APPLICATION_JSON).content(jsonUserSignUp))
                .andReturn().getResponse().getContentAsString();

        String jsonUserSignIn = "{\n" +
                "\"username\":\"test\",\n" +
                "\"password\":\"12345678\"\n" +
                "}";

        String response = mvc.perform(post("/api/auth/signin")
                .contentType(APPLICATION_JSON_UTF8).content(jsonUserSignIn))
                .andExpect(status().isOk()).andReturn().getResponse().getContentAsString();

        ObjectMapper objectMapper = new ObjectMapper();
        JsonNode actualObj = objectMapper.readTree(response);

        token = "Bearer " + actualObj.get("accessToken").asText();

    }

    @Test
    public void listAll() throws Exception {
        Shipment shipment = new Shipment();
        shipment.setCompleted(false);

        ShipmentDTO firstDto = shipmentService.save(mapper.map(shipment, ShipmentDTO.class));
        ShipmentDTO secondDto = shipmentService.save(mapper.map(shipment, ShipmentDTO.class));

        List<ShipmentDTO> shipmentDTOList = new ArrayList<>();
        shipmentDTOList.add(firstDto);
        shipmentDTOList.add(secondDto);

        String jsonObjects = new ObjectMapper().writeValueAsString(shipmentDTOList);

        String response = mvc.perform(get("/api/shipment/all").header("Authorization", token))
                .andExpect(status().isOk()).andReturn().getResponse().getContentAsString();

        assertThat(response).isEqualTo(jsonObjects);
    }

    @Test
    public void getById() throws Exception {
        Shipment shipment = new Shipment();
        shipment.setCompleted(false);

        ShipmentDTO shipmentDTO = shipmentService.save(mapper.map(shipment, ShipmentDTO.class));
        String jsonObjects = new ObjectMapper().writeValueAsString(shipmentDTO);

        String response = mvc.perform(get("/api/shipment/{id}", shipmentDTO.getId()).header("Authorization", token))
                .andExpect(status().isOk()).andReturn().getResponse().getContentAsString();

        assertThat(response).isEqualTo(jsonObjects);
    }

    @Test
    public void insert() throws Exception {
        Shipment shipment = new Shipment();
        shipment.setCompleted(false);

        ShipmentDTO shipmentDTO = shipmentService.save(mapper.map(shipment, ShipmentDTO.class));
        String jsonObject = new ObjectMapper().writeValueAsString(shipmentDTO);

        String response = mvc.perform(post("/api/shipment").contentType(APPLICATION_JSON_UTF8).content(jsonObject).header("Authorization", token))
                .andExpect(status().isOk()).andReturn().getResponse().getContentAsString();

        shipmentDTO.setId(objectMapper.readTree(response).get("id").asLong());
        String jsonObjectWithId = new ObjectMapper().writeValueAsString(shipmentDTO);

        assertThat(response).isEqualTo(jsonObjectWithId);
    }

    @Test
    public void delete() throws Exception {
        Shipment shipment = new Shipment();
        shipment.setCompleted(false);

        ShipmentDTO shipmentDTO = shipmentService.save(mapper.map(shipment, ShipmentDTO.class));

        String response = mvc.perform(MockMvcRequestBuilders.delete("/api/shipment/{id}", shipmentDTO.getId()).header("Authorization", token))
                .andExpect(status().isOk()).andReturn().getResponse().getContentAsString();

        assertThat(response).isEqualTo("Shipment with id:" + shipmentDTO.getId() + " has been deleted");
    }

    @Test
    public void update() throws Exception {
        Shipment shipment = new Shipment();
        shipment.setCompleted(false);

        ShipmentDTO shipmentDTO = shipmentService.save(mapper.map(shipment, ShipmentDTO.class));
        shipmentDTO.setCompleted(true);
        String jsonObject = new ObjectMapper().writeValueAsString(shipmentDTO);


        String response = mvc.perform(put("/api/shipment").contentType(APPLICATION_JSON_UTF8).content(jsonObject).header("Authorization", token))
                .andExpect(status().isOk()).andReturn().getResponse().getContentAsString();

        assertThat(response).isEqualTo(jsonObject);
    }
}

