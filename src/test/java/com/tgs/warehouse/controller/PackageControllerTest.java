package com.tgs.warehouse.controller;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.tgs.warehouse.Application;
import com.tgs.warehouse.model.ProductPackage;
import com.tgs.warehouse.model.dto.ProductPackageCreationDTO;
import com.tgs.warehouse.model.dto.ProductPackageUpdateDTO;
import com.tgs.warehouse.security.jwt.authentication.controller.AuthRestAPIs;
import com.tgs.warehouse.security.model.Role;
import com.tgs.warehouse.security.model.RoleName;
import com.tgs.warehouse.security.model.User;
import com.tgs.warehouse.security.repository.RoleRepository;
import com.tgs.warehouse.security.repository.UserRepository;
import com.tgs.warehouse.security.service.UserDetailsServiceImpl;
import com.tgs.warehouse.service.IService;
import jdk.nashorn.internal.parser.JSONParser;
import net.minidev.json.JSONObject;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static com.tgs.warehouse.security.model.RoleName.*;
import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.http.MediaType.APPLICATION_JSON_UTF8;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = Application.class)
@TestPropertySource(
        locations = "classpath:application-test.properties")
@AutoConfigureMockMvc
@Transactional
public class PackageControllerTest {

    private String token;

    @Autowired
    AuthRestAPIs authRestAPIs;


    @Autowired
    RoleRepository roleRepository;



    @Autowired
    PackageController packageController;

    @Autowired
    MockMvc mvc;

    @Autowired
    ModelMapper mapper;

    @Autowired
    IService<ProductPackage> service;

    @Before
    public void beforeTest() throws Exception{
        Role admin=new Role();
        admin.setName(ROLE_ADMIN);
        roleRepository.save(admin);

        Role user=new Role();
        user.setName(ROLE_USER);
        roleRepository.save(user);

      String jsonUserSignUp="{\n" +
                "\t\"name\":\"Test User\",\n" +
                "\t\"username\":\"test\",\n" +
                "\t\"email\":\"test@yahoo.com\",\n" +
                "\t\"role\":[\"admin\"],\n" +
                "\t\"password\":\"12345678\"\n" +
                "}";

        mvc.perform(post("/api/auth/signup")
                .contentType(APPLICATION_JSON).content(jsonUserSignUp))
                .andReturn().getResponse().getContentAsString();

        String jsonUserSignIn="{\n" +
                "\"username\":\"test\",\n" +
                "\"password\":\"12345678\"\n" +
                "}";

        String response = mvc.perform(post("/api/auth/signin")
                .contentType(MediaType.APPLICATION_JSON_UTF8).content(jsonUserSignIn))
                .andExpect(status().isOk()).andReturn().getResponse().getContentAsString();

        ObjectMapper objectMapper=new ObjectMapper();
        JsonNode actualObj = objectMapper.readTree(response);

        token="Bearer "+actualObj.get("accessToken").asText();

    }

    @Test
    public void listAllPackagesTest() throws Exception {
        ProductPackageCreationDTO productPackageCreationDTO = createNewPackageWithInfo();

        ProductPackageUpdateDTO firstPackageUpdateDTO = mapper.map(service.save(productPackageCreationDTO), ProductPackageUpdateDTO.class);
        ProductPackageUpdateDTO secondPackageUpdateDTO = mapper.map(service.save(productPackageCreationDTO), ProductPackageUpdateDTO.class);

        List<ProductPackageUpdateDTO> packageUpdateDTOs = new ArrayList<>();
        packageUpdateDTOs.add(firstPackageUpdateDTO);
        packageUpdateDTOs.add(secondPackageUpdateDTO);
        String jsonObjects = new ObjectMapper().writeValueAsString(packageUpdateDTOs);

        String response = mvc.perform(get("/api/package").header("Authorization",token))
                .andExpect(status().isOk()).andReturn().getResponse().getContentAsString();

        assertThat(response).isEqualTo(jsonObjects);
    }

    @Test
    public void insertTest() throws Exception {
        ProductPackageCreationDTO productPackageCreationDTO = createNewPackageWithInfo();

        ObjectMapper objectMapper = new ObjectMapper();
        String jsonObject = objectMapper.writeValueAsString(productPackageCreationDTO);

        String response = mvc.perform(post("/api/package").contentType(MediaType.APPLICATION_JSON_UTF8).content(jsonObject).header("Authorization",token))
                .andExpect(status().isOk()).andReturn().getResponse().getContentAsString();
        productPackageCreationDTO.setId(objectMapper.readTree(response).get("id").asLong());
        jsonObject = objectMapper.writeValueAsString(productPackageCreationDTO);

        assertThat(response).isEqualTo(jsonObject);
    }

    @Test
    public void updateTest() throws Exception {
        ProductPackageCreationDTO productPackageCreationDTO = createNewPackageWithInfo();

        ProductPackageUpdateDTO packageUpdateDTO = mapper.map(service.save(productPackageCreationDTO), ProductPackageUpdateDTO.class);
        packageUpdateDTO.setDescription("afterupdatedescription");
        ObjectMapper objectMapper = new ObjectMapper();
        String jsonObject = objectMapper.writeValueAsString(packageUpdateDTO);

        String response = mvc.perform(put("/api/package").contentType(APPLICATION_JSON_UTF8).content(jsonObject).header("Authorization",token))
                .andExpect(status().isOk()).andReturn().getResponse().getContentAsString();

        assertThat(response).isEqualTo(jsonObject);

        ProductPackage ppFromDb = service.findOne(packageUpdateDTO.getId());
        packageUpdateDTO = mapper.map(ppFromDb, ProductPackageUpdateDTO.class);
        String jsonPackageFromDb = objectMapper.writeValueAsString(packageUpdateDTO);

        assertThat(jsonPackageFromDb).isEqualTo(jsonObject);
    }

    @Test
    public void deleteTest() throws Exception {
        ProductPackageCreationDTO productPackageCreationDTO = createNewPackageWithInfo();

        ProductPackageUpdateDTO packageUpdateDTO = mapper.map(service.save(productPackageCreationDTO), ProductPackageUpdateDTO.class);

        String response = mvc.perform(delete("/api/package/{id}", packageUpdateDTO.getId()).header("Authorization",token))
                .andExpect(status().isOk()).andReturn().getResponse().getContentAsString();

        assertThat(response).isEqualTo("Package with id:" + packageUpdateDTO.getId() + " has been deleted");
    }

    private ProductPackageCreationDTO createNewPackageWithInfo() {
        ProductPackageCreationDTO productPackageCreationDTO = new ProductPackageCreationDTO();
        productPackageCreationDTO.setDescription("testdescription");
        productPackageCreationDTO.setType("testtype");
        return productPackageCreationDTO;
    }
}
