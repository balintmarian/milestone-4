insert into product_pallet(description) values ('nochildren');
insert into product_pallet(description) values ('onechild');
insert into product_pallet(description) values ('twochildren');
insert into product_pallet(description) values ('test');

insert into product_package(description,type) values ('test1','test1');
insert into product_package(description,type) values ('singlechild','test2');
insert into product_package(description,type) values ('firstchild','test3');
insert into product_package(description,type) values ('secondchild','test4');
