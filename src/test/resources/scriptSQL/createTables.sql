CREATE TABLE product_pallet(
	id serial PRIMARY KEY,
	description VARCHAR(100)
);

CREATE TABLE product_package(
	id serial PRIMARY KEY,
	description VARCHAR(100),
	type VARCHAR(50),
	product_pallet_id BIGINT REFERENCES product_pallet(id) ON DELETE CASCADE
);