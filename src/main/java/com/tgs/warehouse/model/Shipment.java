package com.tgs.warehouse.model;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "shipment")
public class Shipment {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "completed")
    private Boolean completed;

    @OneToMany(cascade = CascadeType.ALL)
    @JoinTable(name = "shipment_detail", joinColumns = {@JoinColumn(name = "shipment_id")}, inverseJoinColumns = {@JoinColumn(name = "product_pallet_id")})
    private Set<ProductPallet> productPallets;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "planned_shipment_id", referencedColumnName = "id")
    private PlannedShipment plannedShipment;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Boolean getCompleted() {
        return completed;
    }

    public void setCompleted(Boolean completed) {
        this.completed = completed;
    }

    public Set<ProductPallet> getProductPallets() {
        return productPallets;
    }

    public void setProductPallets(Set<ProductPallet> productPallets) {
        this.productPallets = productPallets;
    }

    public PlannedShipment getPlannedShipment() {
        return plannedShipment;
    }

    public void setPlannedShipment(PlannedShipment plannedShipment) {
        this.plannedShipment = plannedShipment;
    }

}
