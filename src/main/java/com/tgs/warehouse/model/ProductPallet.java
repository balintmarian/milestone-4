package com.tgs.warehouse.model;


import javax.persistence.*;
import java.util.List;
import java.util.Objects;


@Entity
@Table(name = "product_pallet")
public class ProductPallet {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "description")
    private String description;

    @OneToMany(targetEntity = ProductPackage.class, fetch = FetchType.LAZY, mappedBy = "productPallet")
    private List<ProductPackage> packages;

    @ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinTable(name = "shipment_detail", joinColumns = {@JoinColumn(name = "product_pallet_id")}, inverseJoinColumns = {@JoinColumn(name = "shipment_id")})
    private Shipment shipment;

    public Shipment getShipment() {
        return shipment;
    }

    public void setShipment(Shipment shipment) {
        this.shipment = shipment;
    }


    public ProductPallet() {
    }

    public ProductPallet(String description) {
        this.description = description;
    }

    public ProductPallet(Long id, String description) {
        this.id = id;
        this.description = description;
    }

    public ProductPallet(String description, List<ProductPackage> packages) {
        this.description = description;
        this.packages = packages;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description.toLowerCase();
    }

    public List<ProductPackage> getPackages() {
        return packages;
    }

    public void setPackages(List<ProductPackage> packages) {
        this.packages = packages;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ProductPallet)) return false;
        ProductPallet that = (ProductPallet) o;
        return id.equals(that.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}

