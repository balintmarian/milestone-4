package com.tgs.warehouse.model.dto;

import com.tgs.warehouse.model.ProductPallet;

public class ProductPalletUpdateDTO extends AbstractUpdateDTO<ProductPallet> {
    private Long shipmentId;

    public Long getShipmentId() {
        return shipmentId;
    }

    public void setShipmentId(Long shipmentId) {
        this.shipmentId = shipmentId;
    }
}
