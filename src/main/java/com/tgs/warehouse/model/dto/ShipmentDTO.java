package com.tgs.warehouse.model.dto;

import java.util.Set;

public class ShipmentDTO {
    private Long id;

    private Boolean completed;

    private Set<ProductPalletUpdateDTO> productPallets;

    private Long plannedShipmentId;


    public Set<ProductPalletUpdateDTO> getProductPallets() {
        return productPallets;
    }

    public void setProductPallets(Set<ProductPalletUpdateDTO> productPallets) {
        this.productPallets = productPallets;
    }

    public Long getPlannedShipmentId() {
        return plannedShipmentId;
    }

    public void setPlannedShipmentId(Long plannedShipmentId) {
        this.plannedShipmentId = plannedShipmentId;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Boolean getCompleted() {
        return completed;
    }

    public void setCompleted(Boolean completed) {
        this.completed = completed;
    }
}
