package com.tgs.warehouse.model.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

/*
All classes extending this will not be deserialized with ID, used for creating new T
 */
public abstract class AbstractCreationDTO<T> {

    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    protected Long id;

    private String description;

    public Long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

}
