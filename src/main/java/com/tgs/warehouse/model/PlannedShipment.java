package com.tgs.warehouse.model;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "planned_shipment")
public class PlannedShipment {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    private String customerName;
    @Column(name = "quantity")
    private Long quantity;

    @OneToOne(mappedBy = "plannedShipment",cascade = CascadeType.ALL)
    private Shipment shipment;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public Long getQuantity() {
        return quantity;
    }

    public void setQuantity(Long quantity) {
        this.quantity = quantity;
    }

    public Shipment getShipment() {
        return shipment;
    }

    public void setShipment(Shipment shipment) {
        this.shipment = shipment;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof PlannedShipment)) return false;
        PlannedShipment that = (PlannedShipment) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(customerName, that.customerName) &&
                Objects.equals(quantity, that.quantity) &&
                Objects.equals(shipment, that.shipment);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, customerName, quantity, shipment);
    }
}
