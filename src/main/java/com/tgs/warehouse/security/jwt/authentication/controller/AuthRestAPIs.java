package com.tgs.warehouse.security.jwt.authentication.controller;

import com.tgs.warehouse.security.helpers.AuxUtility;
import com.tgs.warehouse.security.jwt.JwtProvider;
import com.tgs.warehouse.security.jwt.authentication.message.request.UserLoginDTO;
import com.tgs.warehouse.security.jwt.authentication.message.request.CredentialsDTO;
import com.tgs.warehouse.security.jwt.authentication.message.response.JwtResponse;
import com.tgs.warehouse.security.model.User;
import com.tgs.warehouse.security.repository.RoleRepository;
import com.tgs.warehouse.security.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.HashSet;
import java.util.Set;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/auth")
public class AuthRestAPIs {

    AuthenticationManager authenticationManager;

    UserRepository userRepository;

    RoleRepository roleRepository;

    PasswordEncoder encoder;

    JwtProvider jwtProvider;

    @Autowired
    AuxUtility auxUtility;

    @Autowired
    public AuthRestAPIs(AuthenticationManager authenticationManager, UserRepository userRepository, RoleRepository roleRepository, PasswordEncoder encoder, JwtProvider jwtProvider) {
        this.authenticationManager = authenticationManager;
        this.userRepository = userRepository;
        this.roleRepository = roleRepository;
        this.encoder = encoder;
        this.jwtProvider = jwtProvider;
    }


    @PostMapping("/signin")
    public ResponseEntity<?> authenticateUser(@Valid @RequestBody UserLoginDTO loginRequest) {


        String jwt =auxUtility.authenticate(loginRequest.getUsername(),loginRequest.getPassword());


        return ResponseEntity.ok(new JwtResponse(jwt));
    }

    @PostMapping("/signup")
    public ResponseEntity<String> registerUser(@Valid @RequestBody CredentialsDTO signUpRequest) {

        ResponseEntity<String> isDataAvailable = auxUtility.isDataAvailable(signUpRequest.getUsername(), signUpRequest.getEmail());
        if (isDataAvailable != null) {
            return isDataAvailable;
        }
        // Creating user's account
        User user = new User(signUpRequest.getName(), signUpRequest.getUsername(),
                signUpRequest.getEmail(), encoder.encode(signUpRequest.getPassword()));

        user.setRoles(signUpRequest.getRoles());
        userRepository.save(user);


        return ResponseEntity.ok().body("User registered successfully!");
    }
}