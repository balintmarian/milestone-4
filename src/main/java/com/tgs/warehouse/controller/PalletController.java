package com.tgs.warehouse.controller;

import com.tgs.warehouse.model.ProductPallet;
import com.tgs.warehouse.model.dto.AbstractCreationDTO;
import com.tgs.warehouse.model.dto.AbstractUpdateDTO;
import com.tgs.warehouse.model.dto.ProductPalletCreationDTO;
import com.tgs.warehouse.model.dto.ProductPalletUpdateDTO;
import com.tgs.warehouse.service.AuxUtility;
import com.tgs.warehouse.service.IService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;
import java.util.Objects;

import static org.springframework.web.bind.annotation.RequestMethod.*;
@CrossOrigin("*")
@RestController
@RequestMapping(value = "/api/pallet")
public class PalletController {

    private final IService<ProductPallet> productPalletService;

    @Autowired
    public PalletController(IService<ProductPallet> productPalletService) {
        this.productPalletService = Objects.requireNonNull(productPalletService);
    }

    @RequestMapping(method = GET)
    @PreAuthorize("hasRole('USER') or hasRole('ADMIN')")
    public Collection<AbstractUpdateDTO<ProductPallet>> search(@RequestParam(value = "description", required = false) String description) {
        if (AuxUtility.isNullOrEmpty(description)) {
            return productPalletService.findAll();
        }
        return productPalletService.findAllByDescription(description);
    }

    @RequestMapping(method = GET, value = "/{id}")
    @PreAuthorize("hasRole('USER') or hasRole('ADMIN')")
    public ProductPallet findOne(@PathVariable("id") Long id) {
        return productPalletService.findOne(id);
    }

    @RequestMapping(method = DELETE, value = "/{id}")
    @PreAuthorize("hasRole('USER') or hasRole('ADMIN')")
    public String deletePallet(@PathVariable("id") Long id) {

        productPalletService.deleteById(id);

        return "Pallet with id:" + id + " has been deleted";
    }


    @RequestMapping(method = POST)
    @PreAuthorize("hasRole('USER') or hasRole('ADMIN')")
    public AbstractCreationDTO<ProductPallet> insert(@RequestBody ProductPalletCreationDTO productPalletDTO) {

        return productPalletService.save(productPalletDTO);
    }

    @RequestMapping(method = PUT)
    @PreAuthorize("hasRole('USER') or hasRole('ADMIN')")
    public AbstractUpdateDTO<ProductPallet> update(@RequestBody ProductPalletUpdateDTO productPalletDTO) {

        return productPalletService.update(productPalletDTO);

    }
}


