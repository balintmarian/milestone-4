package com.tgs.warehouse.controller;

import com.tgs.warehouse.model.ProductPackage;
import com.tgs.warehouse.model.dto.AbstractCreationDTO;
import com.tgs.warehouse.model.dto.AbstractUpdateDTO;
import com.tgs.warehouse.model.dto.ProductPackageCreationDTO;
import com.tgs.warehouse.model.dto.ProductPackageUpdateDTO;
import com.tgs.warehouse.service.IService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;
import java.util.Objects;

import static org.springframework.web.bind.annotation.RequestMethod.*;
@CrossOrigin("*")
@RestController
@RequestMapping(value = "/api/package")
public class PackageController {

    private final IService<ProductPackage> productPackageIService;

    @Autowired
    public PackageController(IService<ProductPackage> productPackageIService) {
        this.productPackageIService = Objects.requireNonNull(productPackageIService);
    }

    @RequestMapping(method = GET)
    @PreAuthorize("hasRole('USER') or hasRole('ADMIN')")
    public Collection<AbstractUpdateDTO<ProductPackage>> listAll() {

        return productPackageIService.findAll();
    }

    @RequestMapping(method = GET, value = "/{id}")
    @PreAuthorize("hasRole('USER') or hasRole('ADMIN')")
    public ProductPackage findOne(@PathVariable("id") Long id) {

        return productPackageIService.findOne(id);
    }

    @RequestMapping(method = POST)
    @PreAuthorize("hasRole('USER') or hasRole('ADMIN')")
    public AbstractCreationDTO<ProductPackage> insert(@RequestBody ProductPackageCreationDTO productPackageDTO) {

        return productPackageIService.save(productPackageDTO);
    }

    @RequestMapping(method = PUT)
    @PreAuthorize("hasRole('USER') or hasRole('ADMIN')")
    public AbstractUpdateDTO<ProductPackage> update(@RequestBody ProductPackageUpdateDTO productPackageDTO) {

        return productPackageIService.update(productPackageDTO);
    }

    @RequestMapping(method = DELETE, value = "/{id}")
    @PreAuthorize("hasRole('USER') or hasRole('ADMIN')")
    public String delete(@PathVariable("id") Long id) {

        productPackageIService.deleteById(id);

        return "Package with id:" + id + " has been deleted";
    }
}


