package com.tgs.warehouse.controller;

import com.tgs.warehouse.model.dto.ShipmentDTO;
import com.tgs.warehouse.service.ShipmentService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;

import static org.springframework.web.bind.annotation.RequestMethod.*;
@CrossOrigin("*")
@RestController
@RequestMapping(value = "/api/shipment")
public class ShipmentController {
    private ShipmentService service;
    private ModelMapper mapper;

    @Autowired
    public ShipmentController(ShipmentService service, ModelMapper mapper) {
        this.service = service;
        this.mapper = mapper;
    }

    @RequestMapping(method = GET, value = "/all")
    @PreAuthorize("hasRole('USER') or hasRole('ADMIN')")
    public Collection<ShipmentDTO> findAll() {
        return service.findAll();
    }

    @RequestMapping(method = GET, value = "/{id}")
    @PreAuthorize("hasRole('USER') or hasRole('ADMIN')")
    public ShipmentDTO findOne(@PathVariable("id") Long id) {
        return mapper.map(service.findOne(id), ShipmentDTO.class);
    }

    @RequestMapping(method = POST)
    @PreAuthorize("hasRole('USER') or hasRole('ADMIN')")
    public ShipmentDTO insert(@RequestBody ShipmentDTO shipmentDTO) {

        return service.save(shipmentDTO);
    }

    @RequestMapping(method = DELETE, value = "/{id}")
    @PreAuthorize("hasRole('USER') or hasRole('ADMIN')")
    public String deletePallet(@PathVariable("id") Long id) {

        service.deleteById(id);

        return "Shipment with id:" + id + " has been deleted";
    }

    @RequestMapping(method = PUT)
    @PreAuthorize("hasRole('USER') or hasRole('ADMIN')")
    public ShipmentDTO update(@RequestBody ShipmentDTO shipmentDTO) {

        return service.update(shipmentDTO);
    }

    @RequestMapping(method = PUT, value = "/add")
    @PreAuthorize("hasRole('USER') or hasRole('ADMIN')")
    public ShipmentDTO insertPallet(@RequestBody ShipmentDTO shipmentDTO) {

        return service.insertPallet(shipmentDTO);
    }

    @RequestMapping(method = PUT, value = "/remove")
    @PreAuthorize("hasRole('USER') or hasRole('ADMIN')")
    public ShipmentDTO removePallet(@RequestBody ShipmentDTO shipmentDTO) {

        return service.removePallet(shipmentDTO);
    }
}
