package com.tgs.warehouse.controller;

import com.tgs.warehouse.model.dto.PlannedShipmentDTO;
import com.tgs.warehouse.service.PlannedShipmentService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;

import static org.springframework.web.bind.annotation.RequestMethod.*;
@CrossOrigin("*")
@RestController
@RequestMapping(value = "/api/shipmentplan")
public class PlannedShipmentController {


    private final PlannedShipmentService plannedShipmentService;
    private final ModelMapper mapper;

    @Autowired
    public PlannedShipmentController(PlannedShipmentService plannedShipmentService, ModelMapper mapper) {
        this.plannedShipmentService = plannedShipmentService;
        this.mapper = mapper;
    }

    @RequestMapping(method = GET, value = "/all")
    @PreAuthorize("hasRole('USER') or hasRole('ADMIN')")
    public Collection<PlannedShipmentDTO> findAll() {
        return plannedShipmentService.findAll();
    }

    @RequestMapping(method = GET, value = "/{id}")
    @PreAuthorize("hasRole('USER') or hasRole('ADMIN')")
    public PlannedShipmentDTO findOne(@PathVariable("id") Long id) {
        return mapper.map(plannedShipmentService.findOne(id), PlannedShipmentDTO.class);
    }

    @RequestMapping(method = POST)
    @PreAuthorize("hasRole('USER') or hasRole('ADMIN')")
    public PlannedShipmentDTO insert(@RequestBody PlannedShipmentDTO plannedShipmentDTO) {

        return plannedShipmentService.save(plannedShipmentDTO);
    }

    @RequestMapping(method = DELETE, value = "/{id}")
    @PreAuthorize("hasRole('USER') or hasRole('ADMIN')")
    public String deletePallet(@PathVariable("id") Long id) {

        plannedShipmentService.deleteById(id);

        return "Planned Shipment with id:" + id + " has been deleted";
    }

    @RequestMapping(method = PUT)
    @PreAuthorize("hasRole('USER') or hasRole('ADMIN')")
    public PlannedShipmentDTO update(@RequestBody PlannedShipmentDTO plannedShipmentDTO) {

        return plannedShipmentService.update(plannedShipmentDTO);
    }

}
