package com.tgs.warehouse.service;

public class AuxUtility {
    public static boolean isNullOrEmpty(String s) {
        return s == null || s.trim().isEmpty();
    }
}
