package com.tgs.warehouse.service;

import com.tgs.warehouse.exceptions.ServiceException;
import com.tgs.warehouse.model.PlannedShipment;
import com.tgs.warehouse.model.ProductPallet;
import com.tgs.warehouse.model.Shipment;
import com.tgs.warehouse.model.dto.ProductPalletUpdateDTO;
import com.tgs.warehouse.model.dto.ShipmentDTO;
import com.tgs.warehouse.repository.PlannedShipmentRepository;
import com.tgs.warehouse.repository.ProductPalletRepository;
import com.tgs.warehouse.repository.ShipmentRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class ShipmentService {
    private ModelMapper mapper;
    private ShipmentRepository shipmentRepository;
    private PlannedShipmentRepository plannedShipmentRepository;
    private ProductPalletRepository palletRepository;

    @Autowired
    public ShipmentService(ModelMapper mapper, ShipmentRepository shipmentRepository, PlannedShipmentRepository plannedShipmentRepository, ProductPalletRepository palletRepository) {
        this.mapper = mapper;
        this.shipmentRepository = shipmentRepository;
        this.plannedShipmentRepository = plannedShipmentRepository;
        this.palletRepository = palletRepository;
    }

    public Collection<ShipmentDTO> findAll() {
        return shipmentRepository.findAll().stream().map(p -> mapper.map(p, ShipmentDTO.class)).collect(Collectors.toList());
    }

    public Shipment findOne(Long id) {
        return shipmentRepository.findById(id).get();
    }

    public void deleteById(Long id) {
        try {
            shipmentRepository.deleteById(id);
        } catch (Exception e) {
            throw new ServiceException("No Shipment with ID " + id);
        }
    }


    public ShipmentDTO save(ShipmentDTO dto) {
        Shipment shipment = new Shipment();

        shipment.setCompleted(dto.getCompleted());

        shipment = shipmentRepository.save(shipment);
        return mapper.map(shipment, ShipmentDTO.class);

    }

    public void delete(Long id) {
        shipmentRepository.deleteById(id);
    }

    public ShipmentDTO update(ShipmentDTO dto) {
        Shipment shipment;

        Optional<PlannedShipment> optionalPlannedShipment = Optional.empty();
        Optional<Shipment> optionalShipment = shipmentRepository.findById(dto.getId());

        if (dto.getPlannedShipmentId() != null) {
            optionalPlannedShipment = plannedShipmentRepository.findById(dto.getPlannedShipmentId());
        }

        if (optionalShipment.isPresent()) {
            shipment = optionalShipment.get();
            shipment.setCompleted(dto.getCompleted());

            if (optionalPlannedShipment.isPresent()) {

                PlannedShipment plannedShipment = optionalPlannedShipment.get();
                shipment.setPlannedShipment(plannedShipment);
            }
        } else {
            throw new ServiceException("No Shipment with ID " + dto.getId());
        }
        return mapper.map(shipmentRepository.save(shipment), ShipmentDTO.class);
    }

    public ShipmentDTO insertPallet(ShipmentDTO dto) {
        Optional<Shipment> shipment = shipmentRepository.findById(dto.getId());

        if (shipment.isPresent()) {
            Set<ProductPallet> palletList = shipment.get().getProductPallets();

            for (ProductPalletUpdateDTO pU : dto.getProductPallets()) {
                palletList.add(palletRepository.getOne(pU.getId()));
            }
            shipment.get().setProductPallets(palletList);

            return mapper.map(shipmentRepository.save(shipment.get()), ShipmentDTO.class);
        }
        throw new ServiceException("No shipment with ID " + dto.getId());
    }

    public ShipmentDTO removePallet(ShipmentDTO dto) {
        Optional<Shipment> shipment = shipmentRepository.findById(dto.getId());

        if (shipment.isPresent()) {
            Set<ProductPallet> palletList = shipment.get().getProductPallets();

            for (ProductPalletUpdateDTO pU : dto.getProductPallets()) {

                palletList.remove(palletRepository.getOne(pU.getId()));
            }

            shipment.get().setProductPallets(palletList);

            return mapper.map(shipmentRepository.save(shipment.get()), ShipmentDTO.class);
        }

        throw new ServiceException("No shipment with ID " + dto.getId());
    }
}
