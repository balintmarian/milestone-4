package com.tgs.warehouse.service;

import com.tgs.warehouse.exceptions.ServiceException;
import com.tgs.warehouse.model.ProductPackage;
import com.tgs.warehouse.model.ProductPallet;
import com.tgs.warehouse.model.dto.AbstractCreationDTO;
import com.tgs.warehouse.model.dto.AbstractUpdateDTO;
import com.tgs.warehouse.model.dto.ProductPackageCreationDTO;
import com.tgs.warehouse.model.dto.ProductPackageUpdateDTO;
import com.tgs.warehouse.repository.ProductPackageRepository;
import com.tgs.warehouse.repository.ProductPalletRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class ProductPackageService implements IService<ProductPackage> {
    private final ProductPackageRepository repository;
    private final ProductPalletRepository palletRepository;
    private final ModelMapper mapper;

    @Autowired
    public ProductPackageService(ProductPackageRepository repository, ProductPalletRepository palletRepository, ModelMapper mapper) {
        this.repository = repository;
        this.palletRepository = palletRepository;
        this.mapper = mapper;
    }

    @Override
    public List<AbstractUpdateDTO<ProductPackage>> findAll() {
        return repository.findAll().stream().map(p -> mapper.map(p, ProductPackageUpdateDTO.class)).collect(Collectors.toList());
    }

    @Override
    public ProductPackage findOne(Long id) {

        Optional<ProductPackage> maybe = repository.findById(id);
        if (maybe.isPresent()) {
            return maybe.get();
        }
        throw new ServiceException("No ProductPackage with id " + id);
    }

    @Override
    public Collection<AbstractUpdateDTO<ProductPackage>> findAllByDescription(String description) {
        return null;
    }

    @Override
    public void deleteById(Long id) {
        try {
            repository.deleteById(id);
        } catch (Exception e) {
            throw new ServiceException("No ProductPackage with id " + id);
        }
    }

    @Override
    public AbstractCreationDTO<ProductPackage> save(AbstractCreationDTO<ProductPackage> dto) {

        ProductPackage productPackage = mapper.map(dto, ProductPackage.class);

        return mapper.map(repository.save(productPackage), ProductPackageCreationDTO.class);

    }


    @Override
    public AbstractUpdateDTO<ProductPackage> update(AbstractUpdateDTO<ProductPackage> dto) {

        Optional<ProductPackage> optionalPackage = repository.findById(dto.getId());

        if (!optionalPackage.isPresent()) {
            throw new ServiceException("No package with ID " + dto.getId());
        }

        ProductPackage productPackage = optionalPackage.get();

        ProductPackage updatedPackage;

        ProductPackageUpdateDTO packageDTO = (ProductPackageUpdateDTO) dto;
        Long productPalletId = packageDTO.getProductPalletId();
        if (productPalletId != null) {
            Optional<ProductPallet> optionalPallet = Optional.ofNullable(palletRepository.getOne(productPalletId));
            if (optionalPallet.isPresent()) {
                ProductPallet pallet = optionalPallet.get();
                productPackage.setProductPallet(pallet);
                updatedPackage = repository.save(productPackage);
            } else {
                throw new ServiceException("No pallet with ID " + productPalletId);
            }
        } else {
            productPackage.setProductPallet(null);
            productPackage.setDescription(packageDTO.getDescription());
            productPackage.setType(packageDTO.getType());
            updatedPackage = repository.save(productPackage);
        }
        return mapper.map(updatedPackage, ProductPackageUpdateDTO.class);
    }

}
