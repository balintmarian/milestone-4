package com.tgs.warehouse.service;

import com.tgs.warehouse.exceptions.ServiceException;
import com.tgs.warehouse.model.ProductPallet;
import com.tgs.warehouse.model.dto.AbstractCreationDTO;
import com.tgs.warehouse.model.dto.AbstractUpdateDTO;
import com.tgs.warehouse.model.dto.ProductPalletCreationDTO;
import com.tgs.warehouse.model.dto.ProductPalletUpdateDTO;
import com.tgs.warehouse.repository.ProductPalletRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class ProductPalletService implements IService<ProductPallet> {

    private final ProductPalletRepository repository;
    private final ModelMapper mapper;

    @Autowired
    public ProductPalletService(ProductPalletRepository repository, ModelMapper mapper) {
        this.repository = repository;
        this.mapper = mapper;
    }

    @Transactional
    @Override
    public List<AbstractUpdateDTO<ProductPallet>> findAll() {
        return repository.findAll().stream().map(p -> mapper.map(p, ProductPalletUpdateDTO.class)).collect(Collectors.toList());
    }

    @Transactional
    @Override
    public ProductPallet findOne(Long id) {
        Optional<ProductPallet> maybe = repository.findById(id);
        if (maybe.isPresent()) {
            return maybe.get();
        }
        throw new ServiceException("No Pallet with id " + id);
    }

    @Transactional
    @Override
    public List<AbstractUpdateDTO<ProductPallet>> findAllByDescription(String description) {

        return repository.findAllByDescription(description)
                .stream().map(p -> mapper.map(p, ProductPalletUpdateDTO.class))
                .collect(Collectors.toList());
    }

    @Override
    public void deleteById(Long id) {

        try {
            repository.deleteById(id);
        } catch (Exception e) {
            throw new ServiceException("No Pallet with id " + id);
        }
    }

    @Override
    public AbstractCreationDTO<ProductPallet> save(AbstractCreationDTO<ProductPallet> dto) {
        ProductPallet productPallet = new ProductPallet();

        productPallet.setDescription(dto.getDescription());

        return mapper.map(repository.save(productPallet), ProductPalletCreationDTO.class);
    }

    @Override
    public AbstractUpdateDTO<ProductPallet> update(AbstractUpdateDTO<ProductPallet> dto) {
        if (dto.getId() == null) {
            throw new ServiceException("Pallet doesnt have an id");
        } else if (AuxUtility.isNullOrEmpty(dto.getDescription())) {
            throw new ServiceException("Cannot update a pallet with an empty description");
        }
        Optional<ProductPallet> optionalPallet = repository.findById(dto.getId());
        if (!optionalPallet.isPresent()) {
            throw new ServiceException("No pallet with ID " + dto.getId());
        }

        ProductPallet productPallet = optionalPallet.get();
        productPallet.setDescription(dto.getDescription());

        return mapper.map(repository.save(productPallet), ProductPalletUpdateDTO.class);
    }
}
