package com.tgs.warehouse.service;

import com.tgs.warehouse.exceptions.ServiceException;
import com.tgs.warehouse.model.PlannedShipment;
import com.tgs.warehouse.model.dto.PlannedShipmentDTO;
import com.tgs.warehouse.repository.PlannedShipmentRepository;
import org.apache.commons.lang3.ObjectUtils;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class PlannedShipmentService {
    private ModelMapper mapper;
    private PlannedShipmentRepository plannedShipmentRepository;

    @Autowired
    public PlannedShipmentService(ModelMapper mapper, PlannedShipmentRepository plannedShipmentRepository) {
        this.mapper = mapper;
        this.plannedShipmentRepository = plannedShipmentRepository;
    }


    public Collection<PlannedShipmentDTO> findAll() {
        return plannedShipmentRepository.findAll().stream().map(p -> mapper.map(p, PlannedShipmentDTO.class)).collect(Collectors.toList());
    }


    public PlannedShipment findOne(Long id) {
        return plannedShipmentRepository.findById(id).get();
    }


    public void deleteById(Long id) {
        try {
            plannedShipmentRepository.deleteById(id);
        } catch (Exception e) {
            throw new ServiceException("No PlannedShipment with ID " + id);
        }
    }


    public PlannedShipmentDTO save(PlannedShipmentDTO dto) {
        PlannedShipment plannedShipment = new PlannedShipment();

        plannedShipment.setCustomerName(dto.getCustomerName());
        plannedShipment.setQuantity(dto.getQuantity());
        plannedShipment = plannedShipmentRepository.save(plannedShipment);
        return mapper.map(plannedShipment, PlannedShipmentDTO.class);
    }


    public PlannedShipmentDTO update(PlannedShipmentDTO dto) {
        Optional<PlannedShipment> optionalPlannedShipment = plannedShipmentRepository.findById(dto.getId());

        if (optionalPlannedShipment.isPresent()) {
            PlannedShipment plannedShipment = optionalPlannedShipment.get();

            if (ObjectUtils.allNotNull(dto.getCustomerName().trim()) && !ObjectUtils.isEmpty(dto.getCustomerName().trim())) {
                plannedShipment.setCustomerName(dto.getCustomerName());
            }

            if (ObjectUtils.allNotNull(dto.getQuantity()) || ObjectUtils.isNotEmpty(dto.getQuantity())) {
                plannedShipment.setQuantity(dto.getQuantity());
            }
            return mapper.map(plannedShipmentRepository.save(plannedShipment), PlannedShipmentDTO.class);
        }
        throw new ServiceException("No planned shipment with ID " + dto.getId());

    }
}
