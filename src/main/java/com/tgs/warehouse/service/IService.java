package com.tgs.warehouse.service;

import com.tgs.warehouse.model.dto.AbstractCreationDTO;
import com.tgs.warehouse.model.dto.AbstractUpdateDTO;

import java.util.Collection;


public interface IService<T> {
    Collection<AbstractUpdateDTO<T>> findAll();

    T findOne(Long id);

    Collection<AbstractUpdateDTO<T>> findAllByDescription(String description);

    void deleteById(Long id);

    AbstractCreationDTO<T> save(AbstractCreationDTO<T> dto);

    AbstractUpdateDTO<T> update(AbstractUpdateDTO<T> dto);

}
