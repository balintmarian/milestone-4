package com.tgs.warehouse.repository;

import com.tgs.warehouse.model.ProductPackage;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProductPackageRepository extends JpaRepository<ProductPackage, Long> {
}
