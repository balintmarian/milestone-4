package com.tgs.warehouse.repository;

import com.tgs.warehouse.model.ProductPallet;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Collection;

@Repository
public interface ProductPalletRepository extends JpaRepository<ProductPallet, Long> {
    @Query(value = "" +
            " select pp " +
            " from ProductPallet pp" +
            " left join pp.packages as pk" +
            " where pp.description like %:description%" +
            " or pk.description like %:description%" +
            " group by pp,pp.shipment "
    )
    Collection<ProductPallet> findAllByDescription(@Param("description") String description);

    void deleteById(Long id);

}
