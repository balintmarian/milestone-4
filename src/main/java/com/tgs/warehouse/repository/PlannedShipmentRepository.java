package com.tgs.warehouse.repository;

import com.tgs.warehouse.model.PlannedShipment;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PlannedShipmentRepository extends JpaRepository<PlannedShipment, Long> {
}
