document.addEventListener('DOMContentLoaded', function () {
    submit();
}, false);

var submit = function () {
    console.log("submit()");
    var id = document.getElementById("id").value;
    console.log(id);

    $.get("/api/pallet", {description: id}, function (responseJson) {
        console.log(responseJson);
        var $table = $("#table").empty();
        $table = $("#table").appendTo($("#somediv"));
        $("<th style='text-align: center'>").appendTo($table).text("ID");
        $("<th style='text-align: center'>").appendTo($table).text("Description");
        $("<th style='text-align: center'>").appendTo($table).text("Options");
        $.each(responseJson, function (index, product) {
            console.log(product);
            $("<tr >").appendTo($table)
                .append($("<td align=\"center\">").text(product.id))
                .append($("<td align=\"center\">").text(product.description))
                .append($("<td align=\"center\">")
                    .append('<input id="deleteBtn" type="button" value="Delete" onclick="remove(this, ' + product.id + ')">'));
        });
    })
};

function remove(btn, id) {
    console.log(id);
    $(btn).closest('tr').remove();
    $.ajax({
        url: '/api/pallet/' + id,
        type: 'DELETE',
        dataType: 'json',
        success: function (response) {

            console.log(response.toString());

            $("#footer").text(response);
        }
    });
}