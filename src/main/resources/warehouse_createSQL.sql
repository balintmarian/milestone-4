--Milestone-3 tables <--------------------------------------
CREATE TABLE product_pallet(
	id bigint PRIMARY KEY,
	description VARCHAR(100)
);

CREATE TABLE product_package(
	id bigint PRIMARY KEY,
	description VARCHAR(100),
	type VARCHAR(50),
	product_pallet_id BIGINT REFERENCES product_pallet(id) ON DELETE CASCADE
);

-- Milestone-4 tables <--------------------------------------
CREATE TABLE planned_shipment(
	id bigint PRIMARY KEY,
	customer_name VARCHAR(50),
	completed BIGINT
);

CREATE TABLE shipment(
	id bigint PRIMARY KEY,
	planned_shipment_id BIGINT REFERENCES planned_shipment(id) ON DELETE CASCADE,
	completed BOOLEAN
);

CREATE TABLE shipment_detail(
	shipment_id BIGINT REFERENCES shipment(id) ON DELETE CASCADE,
	product_pallet_id BIGINT REFERENCES product_pallet(id) ON DELETE CASCADE
);
	